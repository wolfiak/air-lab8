/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import newpackage.Lista.typ;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;

/**
 *
 * @author pacio
 */
public class Lista {
    public static List<Object> bank=new ArrayList<>();
    typ t;
    private int licznik=0;
    public Lista(typ t){
        this.t=t;
    }
    private  List<String> lista=new ArrayList<String>();
    public void Dodaj(String s){
        lista.add(s);
    }
    public Document wykonaj(Document doc) throws FileNotFoundException{
        if(doc==null){
            doc=new Document();
        }
        for(String s: lista){
            
            char[] tab=new char[s.length()];
            for(int n=0;n<s.length();n++){
                tab[n]=s.charAt(n);
                if(tab[n]==' '){
                    tab[n]=',';
                }
            }
            String tmp="";
            for(int n=0;n<tab.length;n++){
                tmp+=tab[n];
            }
            s=tmp;
            PrintWriter out=new PrintWriter("C:\\Users\\pacio\\OneDrive\\Dokumenty\\NetBeansProjects\\LuceneWebSearchEngine\\web\\WEB-INF\\Data\\s"+licznik+".txt");
            out.println(s);
            out.close();
          if(t==typ.Text){
              doc.add(new TextField(s,s,Field.Store.YES));
              System.out.println("DODAJE S:"+s);
          }else if(t==typ.Stringo){
              doc.add(new StringField(s, s, Field.Store.YES));
              System.out.println("DODAJE S:"+s);
          }else if(t==typ.Stored){
              System.out.println("DODAJE S:"+s);
              doc.add(new StoredField(s, s));
          }
           licznik++;
        }
       
        return doc;
    }
    
    public enum typ{
    Text,
    Stringo,
    Stored
}
    
}
