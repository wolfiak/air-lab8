package com.tutorialspoint.lucene;

import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import newpackage.Lista;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

public class Indexer {

   private IndexWriter writer;

   public Indexer(String indexDirectoryPath) throws IOException{
      //this directory will contain the indexes
      Directory indexDirectory = 
         FSDirectory.open(new File(indexDirectoryPath).toPath());
      
        IndexWriterConfig config = new IndexWriterConfig(new StandardAnalyzer());
        //create the indexer
        writer = new IndexWriter(indexDirectory, config);
   }

   public void close() throws CorruptIndexException, IOException{
      writer.close();
   }

   private Document getDocument(File file) throws IOException{
      Document document = new Document();

      //index file contents
      Field contentField = new TextField(LuceneConstants.CONTENTS, 
         new FileReader(file));
      //index file name
      Field fileNameField = new StoredField(LuceneConstants.FILE_NAME,
         file.getName());
      //index file path
      Field filePathField = new StoredField(LuceneConstants.FILE_PATH,
         file.getCanonicalPath());

      document.add(contentField);
      document.add(fileNameField);
      document.add(filePathField);

      return document;
   }   

   private void indexFile(File file) throws IOException{
      System.out.println("Indexing "+file.getCanonicalPath());
      Document document = getDocument(file);
      for(Object o : Lista.bank){
          Lista l =(Lista)o;
          document=l.wykonaj(document);
      }
      System.out.println("Przed dodaniem");
      writer.addDocument(document);
   }

   public int createIndex(String dataDirPath, FileFilter filter) 
      throws IOException{
      //get all files in the data directory
      File[] files = new File(dataDirPath).listFiles();
      Document lol=new Document();
      for(Object o : Lista.bank){
          Lista l =(Lista)o;
          lol=l.wykonaj(lol);
      }
      for( File f :files){
          System.out.println(f.getAbsolutePath());
      }
      for (File file : files) {
         if(!file.isDirectory()
            && !file.isHidden()
            && file.exists()
            && file.canRead()
            && filter.accept(file)
         ){
            indexFile(file);
         }
      }
      return writer.numDocs();
   }
}