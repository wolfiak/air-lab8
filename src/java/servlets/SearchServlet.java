/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import com.sun.jndi.toolkit.url.Uri;

import com.tutorialspoint.lucene.LuceneConstants;
import com.tutorialspoint.lucene.Searcher;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.net.URL;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;

/**
 *
 * @author Marcin
 */
public class SearchServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ServletConfig config=getServletConfig();  
        ServletContext scxt = config.getServletContext();
        String initParam = config.getInitParameter("../com/tutorialspoint/lucene/Indexer");
        URL webinfPath = scxt.getResource("/../../WEB-INF/Data");
        String indexDirectoryPath ="C:\\Users\\pacio\\OneDrive\\Dokumenty\\NetBeansProjects\\LuceneWebSearchEngine\\web\\WEB-INF\\dupa";//=Uri.parse("/../../WEB-INF/Data/");

        String queryParam = request.getParameter("query")!=null?request.getParameter("query"):"";
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Search Engine</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Search Engine</h1>");
            out.println("<p>Example query: Mohan</p>");
            out.println("<form id=\"searchForm\" method=\"post\">\n" +
                        "	<input type=\"text\" name=\"query\" value=\""+queryParam+"\" />\n" +
                        "	<input type=\"submit\" value=\"Search\" />\n" +
                        "</form>");
            if(!queryParam.isEmpty())
            {
                
                Searcher searcher=null;
                try {
                    searcher = new Searcher(indexDirectoryPath);
                    long startTime = System.currentTimeMillis();
                    TopDocs hits;
                    hits = searcher.search(queryParam);
                    long endTime = System.currentTimeMillis();

                    out.println(hits.totalHits
                            + " documents found. Time :" + (endTime - startTime) + "<br />");
                    for (ScoreDoc scoreDoc : hits.scoreDocs) {
                        Document doc = searcher.getDocument(scoreDoc);
                        out.println("File: "
                                + doc.get(LuceneConstants.FILE_PATH) + "<br />");
                    }
                } catch (ParseException ex) {
                    ex.printStackTrace();
                } finally {
                    if (searcher != null) {
                        searcher.close();
                    }
                }

            }

            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
